package com.bankuish.challenge.controller;

import com.bankuish.challenge.dto.Character;
import com.bankuish.challenge.dto.LogInfo;
import com.bankuish.challenge.repository.LogInfoRepository;
import com.bankuish.challenge.service.RickAndMortyApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
public class RickAndMortyController {

    @Autowired
    private RickAndMortyApiService rickAndMortyApiService;

    @Autowired
    private LogInfoRepository logInfoRepository;

    @GetMapping("/api/v1/random_character")
    public List<Character> characterResponse(HttpServletRequest request) throws IOException {
        List<Character> characters = rickAndMortyApiService.getCharacters(5);
        loggingInfo(request.getRemoteHost(), request.getRequestURL().toString(), characters);
        return characters;
    }

    @GetMapping("/api/v1/random_character/{random_number}")
    public List<Character> characterResponse(@PathVariable("random_number") Integer random_number, HttpServletRequest request) throws IOException {
        if (random_number>5) random_number = 5;
        List<Character> characters = rickAndMortyApiService.getCharacters(random_number);
        loggingInfo(request.getRemoteHost(), request.getRequestURL().toString(), characters);
        return characters;
    }

    @GetMapping("/api/v1/log")
    public Iterable<LogInfo> logInfo() throws IOException {
        Iterable<LogInfo> logInfos = logInfoRepository.findAll();
        return logInfos;
    }

    private void loggingInfo(String remoteHost, String requestUrl, List<Character> characters){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        LogInfo logInfo = new LogInfo();
        logInfo.setCreatedAt(calendar.getTime());
        logInfo.setRemoteHost(remoteHost);
        logInfo.setRequestUrl(requestUrl);
        logInfo.setRequestData(characters.toString());
        logInfoRepository.save(logInfo);
    }
}
