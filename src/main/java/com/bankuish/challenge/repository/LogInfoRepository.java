package com.bankuish.challenge.repository;

import com.bankuish.challenge.dto.LogInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LogInfoRepository extends CrudRepository<LogInfo, Long> {
}
