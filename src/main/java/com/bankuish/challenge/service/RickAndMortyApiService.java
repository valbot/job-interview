package com.bankuish.challenge.service;

import com.bankuish.challenge.dto.Character;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class RickAndMortyApiService {

    private static List<Character> characters;

    public List<Character> getCharacters(int randomNumber) throws IOException {
        List<Integer> randomList = generateRandomNumberList(randomNumber);
        ObjectMapper mapper = new ObjectMapper();
        String requestUrl = generateUrl(randomList);
        HttpGet request = new HttpGet(requestUrl);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
                characters = mapper.readValue(json, new TypeReference<List<Character>>(){});
            }

        }
        return characters;
    }

    private List<Integer> generateRandomNumberList(int randomNumber){
        List<Integer> randomList = new ArrayList<Integer>();
        for (int i = 0; i < randomNumber; i++) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, 691 + 1);
            randomList.add(randomNum);
        }
        return randomList;
    }

    private String generateUrl(List<Integer> randomNumber){
        String param="";
        for(Integer i: randomNumber){
            param = param + i.toString() + ",";
        }
        return "https://rickandmortyapi.com/api/character/"+param;
    }
}
