package com.bankuish.challenge.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_info")
public class LogInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "remote_host")
    private String remoteHost;

    @Column(name = "request_url")
    private String requestUrl;

    @Column(name ="request_data")
    private String requestData;

    public LogInfo() {
    }

    public int getId() {
        return id;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String request) {
        this.requestData = request;
    }

    @Override
    public String toString() {
        return "LogInfo{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", remoteHost='" + remoteHost + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", requestData='" + requestData + '\'' +
                '}';
    }
}
