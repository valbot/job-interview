# Challenge documentation

## Requirements
1. The microservices will retrieve up to 5 random Rick and morty characters sorted by popularity from [https://rickandmortyapi.com](https://rickandmortyapi.com)
2. Any number of characters can be excluded using the character id
3. The popularity index must be included in the response along with all character information 
4. All the request should be stored in order to analyze which kind of series are sent by the user

## Solution Design

![solution_target](./resources/solution_target.png)

1. The microservices expose 3 Rest API:<br>
   
    **A) FIRST API** [http://localhost:8080/api/v1/random_character](http://localhost:8080/api/v1/random_character) retrieve 5 random characters<br>**B) SECOND API** [[http://localhost:8080/api/v1/random_character/{random_number}](http://localhost:8080/api/v1/random_character/3) retrieve {random_number} characters.
    
    **C) THIRD API** [http://localhost:8080/api/v1/log](http://localhost:8080/api/v1/log) retrieve all interaction saved
    
2. A Postgres Container is used to store requests about all interactions.
## How to run the microservice
to execute the microservice locally on your machine:

1. Install Docker
2. Install and configure Java and Maven
3. Open the terminal
4. With the terminal open the project folder, where is located the file "docker-compose.yaml"
5. Lunch the command ```mvn package```
6. To run the microservices lunch the command  ```docker-compose up -d```
7. To release the microservice lunch the command ```docker-compose down```

## What I should do to improve the application
1. Automatically push and deploy the image on a private registry (Example: Docker Hub, AWS Registry)
2. Implementing JUnit Test
3. Expose swagger interface
4. Register the microservice endpoint into API Gateway
5. Implement a JWT authorization and authentication method. The JWT Token allows extracting the identity of the caller 
6. The database could be handled as a PaaS service and not as a Container
7. Implementing a stream processing platform, like Kafka, could help to decouple the microservice from the DB
8. To make the application more fault tolerant could be useful to implement a circuit breaker when the external API is called
9. Implement Jenkins pipeline for automated deployment of the microservice.
10. Implement Exception and Error Handling
11. The connection strings of the microservice should be not hardcoded 