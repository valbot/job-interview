FROM java:8
WORKDIR /
ADD ./target/challenge-1.0.0.jar challenge.jar
EXPOSE 8080
CMD java -jar challenge.jar