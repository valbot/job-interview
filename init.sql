CREATE TABLE public."log_info"
(
    id integer NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    "remote_host" text  NOT NULL,
    "request_url" text,
    "request_data" text NOT NULL,
    CONSTRAINT "logInfo_pkey" PRIMARY KEY (id)
)